
# Django Web-App ![PyPI - Versions from Framework Classifiers](https://img.shields.io/github/license/smahesh29/Django-WebApp) ![Python 3.6, 3.7, 3.8](https://img.shields.io/pypi/pyversions/clubhouse?color=blueviolet)


This is a Python Django website for selling perfumes. It allows users to browse and purchase perfumes from a variety of brands, including popular brands like Chanel, Dior, and Yves Saint Laurent. The website also includes features such as product reviews, a wishlist, and a shopping cart.

## Setting Up

Set the virtual enviroment for further work

```
pip install virtualenv
py -m venv env
./env/Scripts/activate.bat
```

## Installation

Install Django and other libraries 

```
pip install -r requirements.txt
```

## Usage

```
python django_web_app/manage.py makemigrations
python django_web_app/manage.py migrate
python django_web_app/manage.py runserver
```

In your web browser enter the address : http://localhost:8000 or http://127.0.0.1:8000/

## Login

You need to have credentials to be able to log in the admin panel or website account.

You can access the django admin page at http://127.0.0.1:8000/admin and login with username 'admin' and the '12345' password.

Also a new admin user can be created using

```
python manage.py createsuperuser
```

## Run Test Suite

After setting up your development environment you may run tests.

```
python manage.py test django-web-app
```

## Technologies Used

- Python

- Django 

- HTML/CSS

- JavaScript

- Bootstrap

- SQLite 

## License 

This project is licensed under the [MIT License](https://opensource.org/license/mit/). Feel free to use, modify, and distribute it as per the terms of the license.

Thank you for using and contributing to the Python Django Perfume Store Website! If you have any questions or encounter issues, please don't hesitate to reach out to us.